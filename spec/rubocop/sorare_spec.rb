# frozen_string_literal: true

RSpec.describe RuboCop::Sorare do
  it 'has a version number' do
    expect(RuboCop::Sorare::VERSION).not_to be nil
  end
end
