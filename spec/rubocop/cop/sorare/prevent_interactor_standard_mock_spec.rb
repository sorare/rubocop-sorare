# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventInteractorStandardMock, :config do
  it 'registers an offense when using `receive(:call)`' do
    expect_offense(<<~RUBY)
      expect(MyInteractor).to receive(:call).with(args).and_return(returns)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `be_called` or `be_called!`
    RUBY

    expect_correction(<<~RUBY)
      expect(MyInteractor).to be_called.with(args).and_return(returns)
    RUBY
  end

  it 'registers an offense when using `not_to receive(:call)`' do
    expect_offense(<<~RUBY)
      expect(MyInteractor).not_to receive(:call).with(args).and_return(returns)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `be_called` or `be_called!`
    RUBY

    expect_correction(<<~RUBY)
      expect(MyInteractor).not_to be_called.with(args).and_return(returns)
    RUBY
  end

  it 'does not register an offense when using `be_called`' do
    expect_no_offenses(<<~RUBY)
      expect(MyInteractor).to be_called.with(args).and_return(returns)
    RUBY
  end

  it 'does not register an offense when using `not_to be_called`' do
    expect_no_offenses(<<~RUBY)
      expect(MyInteractor).not_to be_called.with(args).and_return(returns)
    RUBY
  end

  it 'does not register an offense when expecting another method than :call' do
    expect_no_offenses(<<~RUBY)
      expect(MyClass).to receive(:get).with(args).and_return(returns)
    RUBY
  end

  context 'with valid receivers' do
    before do
      allow_any_instance_of(RuboCop::Cop::Sorare::PreventInteractorStandardMock).to receive(:valid_receivers).and_return(['Platform::RpcClient'])
    end

    it 'does not register an offense with an allowed receiver' do
      expect_no_offenses(<<~RUBY)
        expect(Platform::RpcClient).to receive(:call).with(args).and_return(returns)
      RUBY
    end
  end
end
