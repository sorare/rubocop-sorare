# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventInteractorDelegateToContext, :config do
  it 'registers an offense when using `delegate :field, to: :context`' do
    expect_offense(<<~RUBY)
      delegate :field, to: :context
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `receive :attribute` or `hold :attribute`
    RUBY
  end

  it 'registers an offense when using `delegate :field, :other_field, to: :context`' do
    expect_offense(<<~RUBY)
      delegate :field, :other_field, to: :context
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `receive :attribute` or `hold :attribute`
    RUBY
  end

  it 'does not register an offense when using `delegate :field, to: :card`' do
    expect_no_offenses(<<~RUBY)
      delegate :field, to: :card
    RUBY
  end
end
