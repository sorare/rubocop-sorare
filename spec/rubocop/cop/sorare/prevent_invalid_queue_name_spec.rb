# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventInvalidQueueName, :config do
  it 'registers an offense when using `queue_as :very_low`' do
    expect_offense(<<~RUBY)
      queue_as :very_low
      ^^^^^^^^^^^^^^^^^^ very_low not in valid queue names verylow, low, default, mailers, high, notify_gql_default, notify_gql_high, screenshot, search
    RUBY
  end

  it 'does not register an offense when using `queue_as :verylow`' do
    expect_no_offenses(<<~RUBY)
      queue_as :verylow
    RUBY
  end
end
