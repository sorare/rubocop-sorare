# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventInGroupsOfWithoutFill, :config do
  it 'registers an offense when using in_groups_of without fill' do
    expect_offense(<<~RUBY)
      array.in_groups_of(42, false)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `each_slice` instead of `in_groups_of` if not filling
    RUBY

    expect_correction(<<~RUBY)
    array.each_slice(42)
    RUBY
  end

  it 'autocorrects on several lines' do
    expect_offense(<<~RUBY)
      array.in_groups_of(
      ^^^^^^^^^^^^^^^^^^^ Use `each_slice` instead of `in_groups_of` if not filling
        42, 
        false
      )
    RUBY

    expect_correction(<<~RUBY)
    array.each_slice(
      42
    )
    RUBY
  end

  it 'does not register an offense when using in_groups_of with fill' do
    expect_no_offenses(<<~RUBY)
    array.in_groups_of(42, nil)
    RUBY
  end
end
