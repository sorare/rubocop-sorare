# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventMemoWithArgs, :config do
  it 'does not register an offense when or-assignment-based memoization is used outside a method definition' do
    expect_no_offenses(<<~RUBY)
      @x ||= y
    RUBY
  end

  let(:cop_config) { { 'EnforcedStyleForLeadingUnderscores' => 'disallowed' } }

  context 'when or-assignment-based memoization is used' do
    context 'method takes one argument' do
      it 'registers an offense' do
        expect_offense(<<~RUBY)
          def x(a)
            @my_var ||= :foo
            ^^^^^^^ You should not try and memoize methods that take arguments.
          end
        RUBY
      end
    end

    context 'method takes one argument and one unused argument' do
      it 'registers an offense' do
        expect_offense(<<~RUBY)
          def x(a, _b)
            @my_var ||= :foo
            ^^^^^^^ You should not try and memoize methods that take arguments.
          end
        RUBY
      end
    end

    context 'method takes one unused argument' do
      it 'registers an offense' do
        expect_no_offenses(<<~RUBY)
          def x(_a)
            @my_var ||= :foo
          end
        RUBY
      end
    end

    context 'method takes two unused argument' do
      it 'registers an offense' do
        expect_no_offenses(<<~RUBY)
          def x(_a, _)
            @my_var ||= :foo
          end
        RUBY
      end
    end

    context 'memoized variable after other code and method take one argument' do
      it 'registers an offense' do
        expect_offense(<<~RUBY)
          def foo(x)
            helper_variable = something_we_need_to_calculate_foo
            @bar ||= calculate_expensive_thing(helper_variable)
            ^^^^ You should not try and memoize methods that take arguments.
          end
        RUBY
      end
    end

    context 'method does not take any argument' do
      it 'does not register an offense' do
        expect_no_offenses(<<~RUBY)
          def x
            @x ||= :foo
          end
        RUBY
      end
    end

    context 'with dynamically defined methods' do
      context 'when the method does not take arguments' do
        it 'does not register an offense' do
          expect_no_offenses(<<~RUBY)
            define_method(:values) do
              @values ||= do_something
            end
          RUBY
        end
      end

      context 'when the method takes arguments' do
        it 'registers an offense' do
          expect_offense(<<~RUBY)
            define_method(:values) do |x|
              @foo ||= do_something
              ^^^^ You should not try and memoize methods that take arguments.
            end
          RUBY
        end
      end

      context 'when a method is defined inside a module callback' do
        context 'when the method does not take arguments' do
          it 'does not register an offense' do
            expect_no_offenses(<<~RUBY)
              def self.inherited(klass)
                klass.define_method(:values) do
                  @values ||= do_something
                end
              end
            RUBY
          end
        end

        context 'when the method takes arguments' do
          it 'registers an offense' do
            expect_offense(<<~RUBY)
              def self.inherited(klass)
                klass.define_method(:values) do |x|
                  @foo ||= do_something
                  ^^^^ You should not try and memoize methods that take arguments.
                end
              end
            RUBY
          end
        end
      end

      context 'when a singleton method is defined inside a module callback' do
        context 'when the method does not take arguments' do
          it 'does not register an offense' do
            expect_no_offenses(<<~RUBY)
              def self.inherited(klass)
                klass.define_singleton_method(:values) do
                  @values ||= do_something
                end
              end
            RUBY
          end
        end

        context 'when the method takes arguments' do
          it 'registers an offense' do
            expect_offense(<<~RUBY)
              def self.inherited(klass)
                klass.define_singleton_method(:values) do |x|
                  @foo ||= do_something
                  ^^^^ You should not try and memoize methods that take arguments.
                end
              end
            RUBY
          end
        end
      end
    end
  end

  context 'when defined?-based memoization is used' do
    it 'registers an offense when the method takes arguments' do
      expect_offense(<<~RUBY)
        def x(y)
          return @my_var if defined?(@my_var)
                 ^^^^^^^ You should not try and memoize methods that take arguments.
                                     ^^^^^^^ You should not try and memoize methods that take arguments.
          @my_var = false
          ^^^^^^^ You should not try and memoize methods that take arguments.
        end
      RUBY
    end

    context 'method does not take arguments' do
      it 'does not register an offense' do
        expect_no_offenses(<<~RUBY)
          def x
            return @x if defined?(@x)
            @x = false
          end
        RUBY
      end

      context 'non-memoized variable in a method that takes arguments' do
        it 'does not register an offense' do
          expect_no_offenses(<<~RUBY)
            def a(y)
              return x if defined?(x)
              x = 1
            end
          RUBY
        end
      end
    end

    context 'with dynamically defined methods' do
      context 'when the method does not take arguments' do
        it 'does not register an offense' do
          expect_no_offenses(<<~RUBY)
            define_method(:values) do
              return @values if defined?(@values)
              @values = do_something
            end
          RUBY
        end
      end

      context 'when the method takes arguments' do
        it 'registers an offense' do
          expect_offense(<<~RUBY)
            define_method(:values) do |x|
              return @foo if defined?(@foo)
                     ^^^^ You should not try and memoize methods that take arguments.
                                      ^^^^ You should not try and memoize methods that take arguments.
              @foo = do_something
              ^^^^ You should not try and memoize methods that take arguments.
            end
          RUBY
        end
      end

      context 'when a method is defined inside a module callback' do
        context 'when the method does not take arguments' do
          it 'does not register an offense' do
            expect_no_offenses(<<~RUBY)
              def self.inherited(klass)
                klass.define_method(:values) do
                  return @values if defined?(@values)
                  @values = do_something
                end
              end
            RUBY
          end
        end

        context 'when the method takes arguments' do
          it 'registers an offense' do
            expect_offense(<<~RUBY)
              def self.inherited(klass)
                klass.define_method(:values) do |x|
                  return @foo if defined?(@foo)
                         ^^^^ You should not try and memoize methods that take arguments.
                                          ^^^^ You should not try and memoize methods that take arguments.
                  @foo = do_something
                  ^^^^ You should not try and memoize methods that take arguments.
                end
              end
            RUBY
          end
        end
      end

      context 'when a singleton method is defined inside a module callback' do
        context 'when the method does not take arguments' do
          it 'does not register an offense' do
            expect_no_offenses(<<~RUBY)
              def self.inherited(klass)
                klass.define_singleton_method(:values) do
                  return @values if defined?(@values)
                  @values = do_something
                end
              end
            RUBY
          end
        end

        context 'when the method takes arguments' do
          it 'registers an offense' do
            expect_offense(<<~RUBY)
              def self.inherited(klass)
                klass.define_singleton_method(:values) do |x|
                  return @foo if defined?(@foo)
                         ^^^^ You should not try and memoize methods that take arguments.
                                          ^^^^ You should not try and memoize methods that take arguments.
                  @foo = do_something
                  ^^^^ You should not try and memoize methods that take arguments.
                end
              end
            RUBY
          end
        end
      end
    end
  end
end
