# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventEventsCreate, :config do
  it 'registers an offense when using `Events::Create.call' do
    expect_offense(<<~RUBY)
      Events::Create.call(argument: value)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `emit!`
    RUBY
  end

  it 'registers an offense when using `Events::Create.call!' do
    expect_offense(<<~RUBY)
      Events::Create.call!(argument: value)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `emit!`
    RUBY
  end

  it 'does not register an offense when using `Events::Listen.call!(argument: value)`' do
    expect_no_offenses(<<~RUBY)
      Events::Listen.call!(argument: value)
    RUBY
  end
end
