# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::EnforceTimestampUpdate, :config do
  it 'registers an offense when using `update_all` without `updated_at` key' do
    expect_offense(<<~RUBY)
      Player.update_all(name: 'Marc Planus')
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ updated_at Timestamp should be updated for data replication
    RUBY
  end

  it 'registers an offense when using `update_all` without `updated_at` key with complex scope' do
    expect_offense(<<~RUBY)
      Player.where(l15: 80).update_all(name: 'Marc Planus')
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ updated_at Timestamp should be updated for data replication
    RUBY
  end

  it 'registers an offense when using `update_all` without `updated_at` in SQL' do
    expect_offense(<<~RUBY)
      Player.update_all('name = Marc Planus')
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ updated_at Timestamp should be updated for data replication
    RUBY
  end

  it 'registers an offense when using `update_all` without `updated_at` key' do
    expect_offense(<<~RUBY)
      player.update_columns(name: 'Marc Planus')
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ updated_at Timestamp should be updated for data replication
    RUBY
  end

  it 'registers an offense when using `update_all` without `updated_at` key with variable key' do
    expect_no_offenses(<<~RUBY)
      col = 'name'
      player.update_columns(col => 'Marc Planus', updated_at: Time.zone.now)
    RUBY
  end

  it 'does not register an offense when using `update_all` with a timestamp key' do
    expect_no_offenses(<<~RUBY)
      Player.update_all(name: 'Mickaël Ciani', updated_at: Time.zone.now)
    RUBY
  end

  it 'does not register an offense when using `update_all` with a timestamp in SQL' do
    expect_no_offenses(<<~RUBY)
      Player.update_all("name = 'Mickaël Ciani', updated_at = NOW()")
    RUBY
  end

  it 'does not register an offense when using `update_columns` with a timestamp key' do
    expect_no_offenses(<<~RUBY)
      player.update_columns(name: 'Mickaël Ciani', updated_at: Time.zone.now)
    RUBY
  end
end
