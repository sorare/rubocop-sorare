# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventExpectingEventsCreate, :config do
  it 'registers an offense when using `expect(Events::Create).to be_called!`' do
    expect_offense(<<~RUBY)
      expect(Events::Create).to be_called!.with(args)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `expect { subject }.to have_published(MyEvent, event_type).with(params)`
    RUBY
  end

  it 'registers an offense when using `expect(Events::Create).not_to be_called!`' do
    expect_offense(<<~RUBY)
      expect(Events::Create).not_to be_called!.with(args)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `expect { subject }.to have_published(MyEvent, event_type).with(params)`
    RUBY
  end
end
