# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Sorare::PreventExpectingCallAsync, :config do
  it 'registers an offense when using `receive(:call_async)`' do
    expect_offense(<<~RUBY)
      expect(MyInteractor).to receive(:call_async).with(args)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `expect { subject }.to have_scheduled(MyInteractor).with(args)`
    RUBY
  end

  it 'registers an offense when using `not_to receive(:call_async)`' do
    expect_offense(<<~RUBY)
      expect(MyInteractor).not_to receive(:call_async).with(args)
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `expect { subject }.to have_scheduled(MyInteractor).with(args)`
    RUBY
  end

  it 'registers an offense when using `have_enqueued_job(InteractorJob)`' do
    expect_offense(<<~RUBY)
      expect { context }.to have_enqueued_job(InteractorJob).with(
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `expect { subject }.to have_scheduled(MyInteractor).with(args)`
        'MyInteractor', player: player
      )
    RUBY
  end

  it 'registers an offense when using `have_enqueued_job(InteractorUniqueJob)`' do
    expect_offense(<<~RUBY)
      expect { context }.to have_enqueued_job(InteractorUniqueJob).with(
      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Use `expect { subject }.to have_scheduled(MyInteractor).with(args)`
        'MyInteractor', player: player
      )
    RUBY
  end

  it 'does not register an offense when using `have_enqueued_job(MyJob)`' do
    expect_no_offenses(<<~RUBY)
      expect { subject }.to have_enqueued_job(MyJob).with(args)
    RUBY
  end

  it 'does not register an offense when using `to schedule`' do
    expect_no_offenses(<<~RUBY)
      expect { subject }.to schedule(MyInteractor).with(args)
    RUBY
  end

  it 'does not register an offense when expecting another method than :call' do
    expect_no_offenses(<<~RUBY)
      expect(MyClass).to receive(:get).with(args).and_return(returns)
    RUBY
  end
end
