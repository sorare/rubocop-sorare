# frozen_string_literal: true

require_relative 'lib/rubocop/sorare/version'

Gem::Specification.new do |spec|
  spec.name          = 'rubocop-sorare'
  spec.version       = RuboCop::Sorare::VERSION
  spec.authors       = ["Sorare's team"]
  spec.email         = ['tech@sorare.com']

  spec.summary       = 'Rubocop custom rules for Sorare'
  spec.homepage      = 'https://gitlab.com/sorare/rubocop-sorare'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/sorare/rubocop-sorare'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'rubocop'
end
