# frozen_string_literal: true

require 'rubocop'

require_relative 'rubocop/sorare'
require_relative 'rubocop/sorare/version'
require_relative 'rubocop/sorare/inject'

RuboCop::Sorare::Inject.defaults!

require_relative 'rubocop/cop/sorare_cops'
