# frozen_string_literal: true

module RuboCop
  module Sorare
    VERSION = '0.1.0'
  end
end
