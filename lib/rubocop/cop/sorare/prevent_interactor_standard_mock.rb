# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `expect(MyInteractor).to receive(:call).with(args).and_return(returns)` does not check that all passed arguments
      # are present and that returns is a Hash.
      #
      # @example
      #   # bad
      #   expect(MyInteractor).to receive(:call).with(args).and_return(returns)
      #
      #   # bad
      #   expect(MyInteractor).to receive(:call!).with(args).and_return(returns)
      #
      #   # bad
      #   expect(MyInteractor).not_to receive(:call!).with(args).and_return(returns)
      #
      #   # good
      #   expect(MyInteractor).to be_called.with(args).and_return(returns)
      #
      #   # good
      #   expect(MyInteractor).to be_called!.with(args).and_return(returns)
      #
      #   # good
      #   expect(MyInteractor).not_to be_called!.with(args).and_return(returns)
      class PreventInteractorStandardMock < Base
        extend AutoCorrector

        MSG = 'Use `be_called` or `be_called!`'

        MAPPING = {
          call: :be_called,
          call!: :be_called!
        }.freeze

        def_node_matcher :mock_interactor?, <<~PATTERN
          (send (send nil? :expect $(...)) {:to :not_to} `$(send nil? :receive (sym ${:call :call!})))
        PATTERN

        def on_send(node)
          mock_interactor?(node) do |receiver|
            next if valid_receivers.include?(receiver.source)

            add_offense(node) do |corrector|
              autocorrect(corrector, node)
            end
          end
        end

        def autocorrect(corrector, node)
          mock_interactor?(node) do |_, call, method|
            corrector.replace(call, MAPPING[method].to_s)
          end
        end

        def valid_receivers
          cop_config.dig('ValidReceivers')
        end
      end
    end
  end
end
