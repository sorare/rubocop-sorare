# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `delegate :attribute, to: :context` is not self documenting enough.
      #
      # @example
      #   # bad
      #   delegate :attribute, to: :context
      #
      #   # good
      #   receive :attribute
      #
      #   # good
      #   hold :attribute
      class PreventEventsCreate < Base
        MSG = 'Use `emit!`'

        def_node_matcher :events_create?, <<~PATTERN
          (send (const (const nil? :Events) :Create) {:call :call!} ...)
        PATTERN

        def on_send(node)
          return unless events_create?(node)

          add_offense(node)
        end
      end
    end
  end
end
