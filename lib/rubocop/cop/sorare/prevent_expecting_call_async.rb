# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `expect(MyInteractor).to receive(:call_async).with(args)` allows expecting a returned value
      #
      # `expect { context }.to have_enqueued_job(InteractorJob).with(
      #   'MyInteractor', player: eq(players.first).or(eq(players.second))
      # ).twice` syntax is awkward and hardcodes the job executor
      #
      # @example
      #   # bad
      #   expect(MyInteractor).to receive(:call_async).with(args)
      #
      #   # bad
      #   expect(MyInteractor).not_to receive(:call_async).with(args)
      #
      #   # bad
      #  `expect { context }.to have_enqueued_job(InteractorJob).with(
      #    'MyInteractor', player: eq(players.first).or(eq(players.second))
      #  ).twice`
      #
      #   # good
      #   expect { subject }.to have_scheduled(MyInteractor).with(args)
      #
      #   # good
      #   expect(MyInteractor).not_to have_scheduled(MyInteractor).with(args)
      class PreventExpectingCallAsync < Base
        MSG = 'Use `expect { subject }.to have_scheduled(MyInteractor).with(args)`'

        def_node_matcher :mock_interactor?, <<~PATTERN
          (send (send nil? :expect ...) {:to :not_to} `(send nil? :receive (sym :call_async)))
        PATTERN

        def_node_matcher :have_enqueued_interactor?, <<~PATTERN
          (send (block (send nil? :expect) _ _) {:to :not_to} `(send nil? :have_enqueued_job (const nil? /Interactor.*Job/)) ...)
        PATTERN

        def on_send(node)
          add_offense(node) if mock_interactor?(node) || have_enqueued_interactor?(node)
        end
      end
    end
  end
end
