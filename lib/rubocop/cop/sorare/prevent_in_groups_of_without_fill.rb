require "rubocop"

module RuboCop
  module Cop
    module Sorare
      class PreventInGroupsOfWithoutFill < Base
        extend AutoCorrector

        DIFF_METH_MSG = "Use `each_slice` instead of `in_groups_of` if not filling".freeze

        def on_send(node)
          return unless in_groups_of_without_fill?(node)

          add_offense(node, message: format(DIFF_METH_MSG, old: name, new: 'each_slice')) { |corrector| autocorrect(corrector, node) }
        end

        def autocorrect(corrector, node)
          return unless in_groups_of_without_fill?(node)

          corrector.replace(node.loc.selector, 'each_slice')
          corrector.remove(node.arguments[0].loc.expression.end.join(node.arguments[1].loc.expression))
        end

        def in_groups_of_without_fill?(node)
          node.method_name == :in_groups_of && node.arguments[1]&.false_type?
        end
      end
    end
  end
end