# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `expect(Events::Create).to receive(:call!).with(args)` is an implementation detail
      #
      # @example
      #   # bad
      #   expect(Events::Create).to receive(:call!).with(args)
      #
      #   # good
      #   expect { subject }.to have_published(MyEvent, event_type).with(params)
      #
      class PreventExpectingEventsCreate < Base
        MSG = 'Use `expect { subject }.to have_published(MyEvent, event_type).with(params)`'

        def_node_matcher :mock_events_create?, <<~PATTERN
          (send (send nil? :expect (const (const nil? :Events) :Create)) {:to :not_to} `(send nil? {:be_called! :be_called}))
        PATTERN

        def on_send(node)
          add_offense(node) if mock_events_create?(node)
        end
      end
    end
  end
end
