# frozen_string_literal: true

module RuboCop
  module Cop
    module Sorare
      # Checks for memoized methods that take arguments.
      # Applies to both regular methods
      # (defined with `def`) and dynamic methods (defined with
      # `define_method` or `define_singleton_method`).
      #
      # @safety
      #   This cop relies on the pattern `@instance_var ||= ...`,
      #   but this is sometimes used for other purposes than memoization
      #   so this cop is considered unsafe.
      #
      # @example
      #   # bad
      #   # Method foo is memoized even though it takes arguments. This can cause confusion and bugs.
      #   def foo(x)
      #     @foot ||= calculate_expensive_thing(x)
      #   end
      #
      #   # bad
      #   def foo(x)
      #     return @foo if defined?(@foo)
      #     @foo = calculate_expensive_thing
      #   end
      #
      #   # good
      #   def foo
      #     @foo ||= calculate_expensive_thing
      #   end
      #
      #   # good
      #   def foo(_)
      #     @foo ||= calculate_expensive_thing
      #   end
      #
      #   # good
      #   def foo
      #     @foo ||= begin
      #       calculate_expensive_thing
      #     end
      #   end
      #
      #   # good
      #   def foo
      #     helper_variable = something_we_need_to_calculate_foo
      #     @foo ||= calculate_expensive_thing(helper_variable)
      #   end
      #
      #   # good
      #   define_method(:foo) do
      #     @foo ||= calculate_expensive_thing
      #   end
      #
      #   # good
      #   define_method(:foo) do
      #     return @foo if defined?(@foo)
      #     @foo = calculate_expensive_thing
      #   end
      #
      class PreventMemoWithArgs < Base
        MSG = 'You should not try and memoize methods that take arguments.'
        DYNAMIC_DEFINE_METHODS = %i[define_method define_singleton_method].to_set.freeze

        # @!method method_definition?(node)
        def_node_matcher :method_definition?, <<~PATTERN
          ${
            (block (send _ %DYNAMIC_DEFINE_METHODS ({sym str} $_)) ...)
            (def $_ ...)
            (defs _ $_ ...)
          }
        PATTERN

        def on_or_asgn(node)
          lhs, _value = *node
          return unless lhs.ivasgn_type?

          method_node, = find_definition(node)
          return unless method_node
          return unless method_uses_arguments?(method_node)

          body = method_node.body
          return unless body == node || body.children.last == node

          add_offense(lhs, message: MSG)
        end

        # @!method defined_memoized?(node, ivar)
        def_node_matcher :defined_memoized?, <<~PATTERN
          (begin
            (if (defined $(ivar %1)) (return $(ivar %1)) nil?)
            ...
            $(ivasgn %1 _))
        PATTERN

        def on_defined?(node)
          arg = node.arguments.first
          return false unless arg.ivar_type?

          method_node, = find_definition(node)
          return false unless method_node
          return false unless method_uses_arguments?(method_node)

          var_name = arg.children.first
          defined_memoized?(method_node.body, var_name) do |defined_ivar, return_ivar, ivar_assign|
            add_offense(defined_ivar, message: MSG)
            add_offense(return_ivar, message: MSG)
            add_offense(ivar_assign.loc.name, message: MSG)
          end
        end

        private

        def method_uses_arguments?(method_node)
          return false unless method_node.arguments.length.positive?

          method_node.arguments.any? { |args| !args.source.start_with?('_') }
        end

        def find_definition(node)
          # Methods can be defined in a `def` or `defs`,
          # or dynamically via a `block` node.
          node.each_ancestor(:def, :defs, :block).each do |ancestor|
            method_node, method_name = method_definition?(ancestor)
            return [method_node, method_name] if method_node
          end

          nil
        end
      end
    end
  end
end
