# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `queue_as` should only target valid queue.
      #
      # @example
      #   # bad
      #   queue_as :very_low
      #
      #   # good
      #   queue_as :verylow
      class PreventInvalidQueueName < Base
        MSG = '%<name>s not in valid queue names %<valid_queue_names>s'

        def_node_matcher :queue_as, <<~PATTERN
          (send nil? :queue_as (:sym $_) ...)
        PATTERN

        def on_send(node)
          queue_as(node) do |name|
            next if valid_queue_names.include?(name.to_s)

            add_offense(node, message: format(MSG, name: name, valid_queue_names: valid_queue_names.join(', ')))
          end
        end

        private

        def valid_queue_names
          cop_config.dig('ValidQueueNames')
        end
      end
    end
  end
end
