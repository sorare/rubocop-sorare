# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `delegate :attribute, to: :context` is not self documenting enough.
      #
      # @example
      #   # bad
      #   delegate :attribute, to: :context
      #
      #   # good
      #   receive :attribute
      #
      #   # good
      #   hold :attribute
      class PreventInteractorDelegateToContext < Base
        MSG = 'Use `receive :attribute` or `hold :attribute`'

        def_node_matcher :delegate_to_context?, <<~PATTERN
          (send nil? :delegate ... (hash (pair (sym :to) (sym :context))))
        PATTERN

        def on_send(node)
          return unless delegate_to_context?(node)

          add_offense(node)
        end
      end
    end
  end
end
