# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Sorare
      # `updated_at column must be updated so that the data pipeline is kept up-to-date
      #
      # @example
      #   # bad
      #   Player.update_all(name: 'Yohan Gourcuff')
      #
      #   # good
      #   Player.update_all(name: 'Yohan Gourcuff', updated_at: Time.zone.now)
      class EnforceTimestampUpdate < Base
        MSG = 'updated_at Timestamp should be updated for data replication'

        def_node_matcher :update_all?, <<~PATTERN
          (send
            _*
            {:update_all :update_columns}
            $_
          )
        PATTERN

        def on_send(node)
          update_all?(node) do |arg|
            add_offense(node) if arg.is_a?(AST::HashNode) && !arg.keys.select do |k|
              k.respond_to?(:value)
            end.map(&:value).include?(:updated_at)
            add_offense(node) if arg.is_a?(AST::StrNode) && !arg.value.include?('updated_at')
          end
        end
      end
    end
  end
end
